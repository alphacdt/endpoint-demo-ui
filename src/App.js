import logo from './logo.svg';
import './App.css';
import DemoPage from './pages/DemoPage';

function App() {
  return (
    <DemoPage/>
  );
}

export default App;
