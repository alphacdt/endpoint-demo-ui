import React, { useState, useEffect } from 'react';
import '../styles/components/tabselector.css';
import Tab from './Tab';

const TabSelector = ({tabs, setActiveTab}) => {
  console.log(tabs)
  return (
    <div className="tabselector">
      {tabs.map((tab, i) => <Tab {...tab} key={i} setActiveTab={setActiveTab}/>)}
    </div>
  );
}

export default TabSelector