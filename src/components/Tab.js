import React, { useState, useEffect } from 'react';
import '../styles/components/tab.css';

const Tab = ({id, title, component, isActive, setActiveTab}) => {
  console.log(title)
  return (
    <div className={`tab ${isActive ? 'active' : ''}`} onClick={()=>setActiveTab(id)}>
      {title}
    </div>
  );
}

export default Tab