import React, { useState, useEffect } from 'react';
import Header from '../components/Header';
import OCR from './OCR';
import TabSelector from '../components/TabSelector';
import '../styles/pages/demopage.css';

const DemoPage = () => {
  const [tabs, setTabs] = useState(
    [
      {
        id: 1,
        title: 'OCR',
        component: 'ocr',
        isActive: true
      },
      {
        id: 2,
        title: 'OCR',
        component: undefined,
        isActive: false
      },
      {
        id: 3,
        title: 'OCR',
        component: undefined,
        isActive: false
      }
    ]
  )

  const getComponent = (component) => {
    switch (component) {
      case 'ocr': 
        return <OCR/>
    }
  }

  const setActiveTab = (id) => {
    const newTabs = JSON.parse(JSON.stringify(tabs));
    newTabs.forEach(tab => {
      tab.id === id ? tab.isActive = true : tab.isActive = false;
    })
    setTabs(newTabs);
  }
  
  return (
    <div className="demopage">
      <Header/>
      <TabSelector tabs={tabs} setActiveTab={setActiveTab}/>
      <div className={'service'}>
        {getComponent(tabs.find(tab => tab.isActive).component)}
      </div>
    </div>
  );
}

export default DemoPage