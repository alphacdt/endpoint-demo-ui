import React, { useState, useEffect } from 'react';
import '../styles/pages/ocrpage.css';
import Dropzone from 'react-dropzone'

const OCR = () => {

  const [file, setFile] = useState(undefined);

  const onFileChange = (e) => {
    setFile(e.target.files[0]);
  }

  const onFileUpload = (files) => {
    const uploadedFile = files[0];
    setFile({
      name: uploadedFile.name,
      file: uploadedFile
    })
  }
  
  return (
    <div className="ocrpage">
      <Dropzone onDrop={acceptedFiles => onFileUpload(acceptedFiles)} maxFiles={1} >
        {({getRootProps, getInputProps}) => (
          <section className={'dropzone'}>
            <div {...getRootProps()}>
              <input {...getInputProps()} />
              <p style={file ? {color: '#4c5967'} : {}}>{file ? file.name : 'Drag file here or click to select a file'}</p>
            </div>
          </section>
        )}
      </Dropzone>
      <div className={`uploadbutton ${file ? '' : 'disabled'}`}>RUN OCR</div>
    </div>
  );
}

export default OCR